**!README!**

**Downloading Cypress and repository with test scenarios**

There are 2 ways to do it:

1. Console mode:
    - Clone or download this repository;
    - In main directory open CMD or PowerShell window;
    - Write command `npm install cypress --save-dev`;

Cypress should be installed as a dependency of our project.


2. Interactive mode:
    - Clone or download this repository;
    - Go [here](https://download.cypress.io/desktop) to download the newest version of Cypress;
    - After download, extract files;
    - Run Cypress.exe;
    - In new window choose option Drag your prject here or select manually;
    - Select main folder of downloaded repository.

In Cypress we should see tree containing prepared test scenarios.

**Downloading Mocha and Allure tools**

In case if we want to generate HTML reports we need to add Mocha and Allure tools - we can do it by installing it via npm.
Before doing it make sure, that we have installd Allure according to this [INSTRUCTION](https://docs.qameta.io/allure/#_installing_a_commandline) 

`npm install mocha --save-dev`
`npm install mocha-allure-reporter --save-dev`
`npm install allure-commandline --save-dev`


**Running test scenarios**

Just like it was in previous part of documentation, we have 2 ways to run scenarios:

1. Console mode:
    - Open CMD or PowerShell window in main directory;
    - Write commend `npx cypress run`;

In console window we should see information about performed tests.
In case if we want to run specific test scenario, we can use comman `npx cypress run --spec "<path_to_spec_js_scenario"`

2. Interactive mode:
    - Open Cypress.exe;
    - Select our project;
    - Press Run all specs button;
 
Then new window should appear where tests will be performed.
In case if we want to run specific test scenario, we can jusr click on scenario from directory tree.

**Generating Allure reports**

In case if we want to generate reports from conducted tests, we need to run cypress with additional parameter `npx cypress run --reporter mocha-allure-reporter`
In order to display reports, we need to type command `allure serve`

**!!ATTENTION!!**

In case of issues with performing tests scenarios, try to do it on Cypress v. 3.2.0.
For console mode - `npm install cypress@3.2.0 --save-dev`.
For interactive mode - download it from [HERE](https://download.cypress.io/desktop/3.2.0?platform=win32&arch=x64).
