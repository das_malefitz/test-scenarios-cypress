/// <reference types="Cypress" />

var { AuthenticationPage, LoginForm } = require('../../support/locators');
var UserType = require('../../models/enums/users')

describe('Test scenario 2.1 - Log in to the application', () => {
    beforeEach(() => {
        cy.visit("http://automationpractice.com/index.php?controller=authentication&back=my-account");
    })

    it("Log in using credentials of existing account", () => {
        cy.login(UserType.valid.email, UserType.valid.password);
        AuthenticationPage.accountNameBtn().contains(UserType.valid.name);
    })

    it("Log in using all invalid credentials", () => {
        cy.login(UserType.invalid.email, UserType.invalid.password);
        LoginForm.errorMessage().should('be.visible').get('ol').contains('Invalid email address');
    })

    it("Log in with invalid email", () => {
        cy.login(UserType.valid.email, UserType.invalid.password);
        LoginForm.errorMessage().should('be.visible').get('ol').contains('Authentication failed');
    })

    it("Log in with invalid password", () => {
        cy.login(UserType.valid.email, UserType.invalid.password);
        LoginForm.errorMessage().should('be.visible').get('ol').contains('Authentication failed');
    })

    it("Log in with no credentials", () => {
        cy.login(UserType.empty.email, UserType.empty.password);
        LoginForm.errorMessage().should('be.visible').get('ol').contains('An email address required');
    })

    it("Log in without email", () => {
        cy.login(UserType.empty.email, UserType.valid.password);
        LoginForm.errorMessage().should('be.visible').get('ol').contains('An email address required');
    })

    it("Log in without password", () => {
        cy.login(UserType.valid.email, UserType.empty.password);
        LoginForm.errorMessage().should('be.visible').get('ol').contains('Password is required');
    })
    
})