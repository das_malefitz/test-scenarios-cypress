/// <reference types="Cypress" />

var { RegistrationForm, AuthenticationPage } = require('../../support/locators');
var EmailType = require('../../models/enums/emails')

describe('Test scenario 1.1 - Email verification', () => {
    beforeEach(() => {
        cy.visit("http://automationpractice.com/index.php?controller=authentication&back=my-account");
    })

    it("Check if user can go to register form after sending valid email", () => {
        cy.mailValidation(EmailType.notUsed.email);
        RegistrationForm.form().should('be.visible');
    })
  
    it("Check if user can go to register form after sending invalid email", () => {
        cy.mailValidation(EmailType.invalid.email);
        RegistrationForm.registerErrorBanner().should('be.visible').get('ol').contains('Invalid email address');
    })

    it("Check if user can go to register form after sending earlier used email", () => {
        cy.mailValidation(EmailType.used.email);
        RegistrationForm.registerErrorBanner().should('be.visible').get('ol').contains('An account using this email address has already been registered');
    })

    it("Check if user can go to register form after sending no email", () => {
        cy.mailValidation(EmailType.empty.email);
        RegistrationForm.registerErrorBanner().should('be.visible').get('ol').contains('Invalid email address');
    })
})