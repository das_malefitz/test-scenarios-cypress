class AuthenticationPage {
    static breadcrumbNavigation = () => cy.get('.breadcrumb');
    static accountNameBtn = () => cy.get('.account');
}

class LoginForm {
    static emailField = () => cy.get('#email');
    static passwordField = () => cy.get('#passwd');
    static signInBtn = () => cy.get('#SubmitLogin > span');
    static errorMessage = () => cy.get('#center_column > :nth-child(2)');
}

class RegistrationForm {
    static emailField = () => cy.get('#email_create');
    static signUpBtn = () => cy.get('#SubmitCreate > span');
    static registerErrorBanner = () => cy.get('#create_account_error');
    static form = () => cy.get('#account-creation_form');
}

module.exports = {
    AuthenticationPage, LoginForm, RegistrationForm
}