var { AuthenticationPage, LoginForm, RegistrationForm } = require('../support/locators')

Cypress.Commands.add('login', (email, password) => {
    AuthenticationPage.breadcrumbNavigation().contains("Authentication");
    LoginForm.emailField().type(email);
    LoginForm.passwordField().type(password);
    LoginForm.signInBtn().click();
})

Cypress.Commands.add('mailValidation', (email) => {
    AuthenticationPage.breadcrumbNavigation().contains("Authentication");
    RegistrationForm.emailField().type(email);
    RegistrationForm.signUpBtn().click();
})