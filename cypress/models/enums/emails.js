const EmailType = Object.freeze({
    "used": { email: "mmaciekpl@gmail.com" },
    "notUsed": { email: "njnfuhg89@onkeypress.co" },
    "invalid": { email: "jkhfdjsfdsfsdkj@90r" },
    "empty": { email: " " },
});

module.exports = EmailType;